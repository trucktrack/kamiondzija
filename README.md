O Putnom nalogu
===========

Putni nalog je papir koji transportna firma izdaje svakom vozaču pre nego što ode na put. Taj papir sadrži informacije o imenu vozača, registraciji kamiona i prikolice i relaciji pošiljke.

Primer kako izgleda popunjen putni nalog ![popunjen putni nalog](http://trucktrack.co/trucktrack-nalog.png)

Funkcionalnosti
===========
Potrebno je kreirati web aplikaciju koja će vlasnicima firmi omogućiti sledeće funkcionalnosti:

* Registracija i prijavljivanje korisnika na sistem
* Popunjavanje i čuvanje putnog naloga
* Pregled Čuvanih putnih naloga

Registracija i prijavljivanje korisnika na sistem
-------------------------------------------------

Prilikom registracije korisnika potrebno je čuvati sledeće informacije:
korisničko ime (email adresa), lozinku, ime firme, adresu firme (ulica i grad) i opciono broj telefona.
Nakon registracije korisnici imaju mogućnost da se prijave na sistem i pristupe ostalim funkcionalnostima.

Izmena podataka korisnika
-------------------------

Korisnik ima mogućnost da izmeni sve podatke unete prilikom registracije, osim korisničkog imena, i da sačuva izmene.

Čuvanje putnog naloga
---------------------

Glavna funkcionalnost aplikacije je čuvanje putnog naloga. 
Prijavljeni korisnik može da tekstualno unese ime vozača, kamion i prikolicu i da unese mesto i datum izdavanja, broj naloga i relaciju pošiljke. Podaci o firmi, datum i mesto bi trebali automatski da se popune podacima firme i trenutnim datumom ali treba da postoji mogućnost da se mesto i datum pošiljke naknadno promene.
Izmena svih podataka naloga bi trebala da bude automatski azurirana i prikazana na samoj stranici u okviru slike putnog naloga.
Nakon unosa svih potrebnih podataka, korisnik ima mogućnost da sačuva popunjeni nalog i pritom generiše fajl u .pdf formatu spreman za štampanje.

Pregled sačuvanih putnih naloga
-------------------------------

Potrebno je pamtiti svaki sačuvani putni nalog i omogućiti korisniku pregled svih prethodno sačuvanih naloga. Korisnik ima mogućnost da preuzme svaki nalog u .pdf formatu za ponovno stampanje.

Otvorene pozicije
==================

Backend
-------
Da bi postao deo java backend tima potrebno je projektovati backend server u **javi** sa proizvoljnom **bazom podataka** (može biti bilo koji JVM jezik, usmislu [java](http://java.com), [scala](http://www.scala-lang.org), [groovy](http://groovy.codehaus.org), za bazu podataka se može koristiti bilo koja: [mysql](http://www.mysql.com/), [postgresql](http://www.postgresql.org/), [mongodb](http://www.mongodb.org/), [java db](http://www.oracle.com/technetwork/java/javadb/overview/index.html), [h2sql](http://www.h2database.com) … bitno je da se pri ponovnom pokretanju servera mogu podaci pročitati). Poželjno je da se projekat builduje [mavenom](http://maven.apache.org) ili [gradleom](http://gradle.org). Takođe potrebno je napraviti **REST servise** pomoću kojih će web projekat komunicirati sa backendom.

Frontend
--------
Da bi postao član frontend tima potrebno je napraviti web aplikaciju. Za web aplikaciju (frontend) obavezno je da se koristi [**angularjs**](http://angularjs.org) framework. Web aplikacija komunicira sa backendom preko RESTa. Sličice se mogu naći [ovde](http://trucktrack.co/jobs/nalog-ui.zip). Za izgled aplikacije pratiti izgled trucktrack.co sajta.

Full stack developer
--------
Ako ti dobro ide i backend i frontend, uradi oba :)

PRIJAVA
-------
Za prijavu je dovoljno da forkuješ projekat na bitbucketu, i kada završiš projekat da pošalješ mail na team@trucktrack.co.